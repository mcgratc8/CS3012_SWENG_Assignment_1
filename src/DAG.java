// The assumption is made that a key value pair will not be altered once they are put in the graph
// and that an edge that would create a cycle is not allowed to be entered into the graph
import java.util.*;

public class DAG<Key extends Comparable<Key>, Value> {

  private int vertices;
  private Bag<GraphNode> nodes;

  @SuppressWarnings("unchecked")
  DAG(){
    this.vertices = 0;
    this.nodes = (Bag<GraphNode>) new Bag();
  }

  private class GraphNode{
		private Key key;           // sorted by key
    private Value val;         // associated data
    public Bag<GraphNode> descendants; //node can have any number of children
    public int inDegree;             // number of nodes in subgraph

    public GraphNode(Key key, Value val) {
      this.key = key;
      this.val = val;
      this.inDegree = 0;
      this.descendants = (Bag<GraphNode>) new Bag();
    }

    public Key key(){return this.key;}

    public Value value(){return this.val;}

    public void addDescendant(GraphNode node){descendants.add(node);}
	}

  //returns whether the passed key exists in the graph
	public boolean exists(Key key){  return get(key)!= null; }

  //returns the number of nodes in the graph
	public int size() {  return nodes.size(); }

	//returns the value of the given key, should it exist in the graph
	public Value get(Key key) {
		Value answer = null;
		if (key != null)
      for(GraphNode node:nodes)
        if(key.equals(node.key()))
          answer = node.val;
		return answer;
	}

  //returns the GraphNode of the given key, should it exist in the graph
	private GraphNode getNode(Key key) {
		GraphNode answer = null;
		if (key != null)
      for(GraphNode node:nodes)
        if(key.equals(node.key()))
          answer = node;
		return answer;
	}

  //inserts a key value pair into the graph if it does not already exist
  public boolean put(Key key, Value val) {
    if (val!= null && !exists(key)){
      nodes.add(new GraphNode(key, val));
      this.vertices++;
      return true;
    }
    return false;
  }

  // adds an edge between two keys in the graph
  public boolean putEdge(Key key1, Key key2){
    if(key1!=null && key2!= null && (!key1.equals(key2))&&(exists(key1) && exists(key2))){
      GraphNode source =  getNode(key1);
      GraphNode dest =  getNode(key2);

      if (!hasEdge(source, dest) && !checkCycle(dest, source)){
        source.addDescendant(dest);
        dest.inDegree++;
        return true;
      }
    }
    return false;
  }

  //checks to see if a cycle has occurred in the graph
  private boolean checkCycle(GraphNode source, GraphNode dest){
    return dfsCycle(source, dest);
  }

  //conducts a depth first search to see if a cycle is in the graph
  private boolean dfsCycle(GraphNode currentNode, GraphNode dest){
    boolean answer = false;
    for (GraphNode node : currentNode.descendants){
      if (node.key().equals(dest.key())) return true;
      answer = answer || dfsCycle(node, dest);
    }
    return false;
  }

  //checks to see if an edge has already been added into the graph
  private boolean hasEdge(GraphNode source, GraphNode dest){
    for(GraphNode node: source.descendants)
      if(node.key.equals(dest.key()))
        return true;
    return false;
  }

  // returns the lowest Common Ancestor of two keys if both exist in the graph.
  // it is assumed that should there be no LCA, the answer is null
  public Set<Key> getLowestCommonAncestor(Key keyA, Key keyB){
    Set<Key> answer = null;
    //there exists a LCA if both the keys are in the directed acyclic graph
    if (exists(keyA) && exists(keyB)){
      //create sets needed for use
      answer = new HashSet<Key>();
      Set<GraphNode> keyANodes = new HashSet<GraphNode>();
      Set<GraphNode> keyBNodes = new HashSet<GraphNode>();
      Set<GraphNode> commonNodes = new HashSet<GraphNode>();

      for(GraphNode node: this.nodes){
        if(node.inDegree == 0){
          //get all ancestors of the keys
          lcaDFS(keyANodes, node, keyA);
          lcaDFS(keyBNodes, node, keyB);
        }
      }
      // if a node is in both sets, add it to the common (intersection) set
      for (GraphNode node: keyANodes){
        if(keyBNodes.contains(node))
          commonNodes.add(node);
      }

      // add nodes in the common set with no children in it into the answer set
      for (GraphNode node: commonNodes){
        boolean hasChild = false;
        for(GraphNode child: node.descendants)
          if(commonNodes.contains(child))
            hasChild = true;
        if(!hasChild) answer.add(node.key());
      }
    }
    return answer;
  }

  //Performs a dfs on the directed acyclic graph for a key,
  //adding them to a set if they are ancestors of the key
  private boolean lcaDFS(Set<GraphNode> set, GraphNode node, Key key){
  	boolean answer = false;
  	if (node.key().equals(key)) {
  	  answer = true;
  	  set.add(node);
  	}
  	else{
  	  for(GraphNode child: node.descendants)
  	    answer |= lcaDFS(set, child, key);
      if(answer)
        set.add(node);
  	}
    return answer;
  }

}
