// The assumption is made that the graph is structured as a "binary tree"
// Class adapted from binary search tree class from Sedgewick and Wayne.
// The assumption is made that a key value pair will not be altered once they are put in the tree
public class LowestCommonAncestor<Key extends Comparable<Key>, Value> {
	LowestCommonAncestor(){
		root = null;
	}

	private TreeNode root;

	private class TreeNode{
		private Key key;           // sorted by key
        private Value val;         // associated data
        private TreeNode left, right;  // left and right subtrees
        private int N;             // number of nodes in subtree

        public TreeNode(Key key, Value val, int N) {
            this.key = key;
            this.val = val;
            this.N = N;
        }
	}

	//returns whether the passed key exists in the tree
	public boolean exists(Key key){
		return get(key) != null;
	}

	//returns the number of nodes below the passed node in the tree
	private int size(TreeNode x) {
        if (x == null) return 0;
        else return x.N;
    }

	//returns the value of the given key, should it exist in the tree
	public Value get(Key key) {
				Value answer = null;
				if (key != null){
					answer = get(root, key);
				}
				return answer;
		}

  private Value get(TreeNode x, Key key) {
        if (x == null) return null;
        int cmp = key.compareTo(x.key);
        if      (cmp < 0) return get(x.left, key);
        else if (cmp > 0) return get(x.right, key);
        else              return x.val;
    }

    //inserts a key value pair into the tree if it does not already exist
	public void put(Key key, Value val) {
    	if (val!= null && !exists(key) ){
				root = put(root, key, val);
			}
		}

    private TreeNode put(TreeNode x, Key key, Value val) {
        if (x == null) return new TreeNode(key, val, 1);
        int cmp = key.compareTo(x.key);
        if      (cmp < 0) x.left  = put(x.left,  key, val);
        else if (cmp > 0) x.right = put(x.right, key, val);
        else              x.val   = val;
        x.N = 1 + size(x.left) + size(x.right); //increment the number of nodes under this node
        return x;
    }

    //prints the keys of the binary tree in order of left to right
    public String treeToString(){
    	return nodeToString(root);
    }
    private String nodeToString(TreeNode node){
    	if(node == null){
    		return "()";
    	}
    	else{
    		return "(" + nodeToString(node.left) + node.key + nodeToString(node.right) + ")";
    		//get keys in order of left and right subtree with current node in centre
    	}
    }

    //returns the lowest common ancestor of two keys if both exist in the tree.
	public Key getLowestCommonAncestor(Key keyA, Key keyB){
		Key answer = null;
		//there exists a LCA if both the keys are in the binary tree
		if (exists(keyA) && exists(keyB)){
			answer = lowestCommonAncestor(root, keyA, keyB);
		}
		return answer;
	}

	private Key lowestCommonAncestor(TreeNode node, Key keyA, Key keyB){
		//if both inputs are greater than the current node, LCA is in right subtree
		if ((node.key.compareTo(keyA)<0 && node.key.compareTo(keyB)<0)){
			return  lowestCommonAncestor(node.right, keyA, keyB);
		}
		//else if both are less, then LCA is in the right subtree
		else if((node.key.compareTo(keyA)>0 && node.key.compareTo(keyB)>0)){
			return  lowestCommonAncestor(node.left, keyA, keyB);
		}
		//otherwise the current node is the LCA
		else{
			return node.key;
		}
	}

}
