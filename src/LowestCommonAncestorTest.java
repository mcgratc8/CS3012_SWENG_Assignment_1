import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;

public class LowestCommonAncestorTest {

	static LowestCommonAncestor<Integer, Integer> lca;
	static DAG<Integer, Integer> dag;

	@BeforeClass
	public static void testEmpty() throws Exception {
		lca  = new LowestCommonAncestor<Integer, Integer>();
		dag  = new DAG<Integer, Integer>();
		//check two common ancestors of empty tree
		assertNull("Checking two common ancestors of empty tree", lca.getLowestCommonAncestor(4, 5));
		assertNull("Checking two common ancestors of empty DAG", dag.getLowestCommonAncestor(4, 5));

		//add data into the tree
		lca.put(4, 4);
		lca.put(3, 3);
		lca.put(7, 7);
		lca.put(6, 6);
		lca.put(9, 9);
		lca.put(10, 10);

		// add data into the dicercted acyclic graph
		dag.put(4, 4);
		dag.put(3, 3);
		dag.put(7, 7);
		dag.put(6, 6);
		dag.put(9, 9);
		dag.put(10, 10);

		dag.putEdge(4,3);
		dag.putEdge(4,7);
		dag.putEdge(7,6);
		dag.putEdge(7,9);
		dag.putEdge(9,10);
	}

	/*	Sample Data used below, for both binary search tree and the directed acyclic graph
	 *  An altered graph with multiple roots is used for some tests
	 *  _4_
	 * 3  _7_
	 *   6   9_
	 *         10_
	 *            20
	*/

	@Test
	public void testPut(){
		//checks to see if the put function operates as needed
		assertEquals("Testing that the keys were correctly inserted into the tree", "((()3())4((()6())7(()9(()10()))))" , lca.treeToString());
		lca.put(20, 20);
		lca.put(null, null);
		assertEquals("Testing that the keys were correctly inserted into the tree", "((()3())4((()6())7(()9(()10(()20())))))" , lca.treeToString());

		//graph testing
		assertEquals("Checking to see if the correct amount of nodes were put into the graph", 6, dag.size());
		assertTrue("Checking insertion works correctly in the graph", dag.put(20, 20));
		assertFalse("Checking null insertion deosn't work in the graph", dag.put(null, null));
		assertEquals("Checking to see if the amount of nodes put into the graph was updated correctly", 7, dag.size());

		assertTrue("Checking edge insertion works correctly in the graph", dag.putEdge(10, 20));
		assertFalse("Checking edge insertion cannot add an edge between a node and itself", dag.putEdge(20, 20));
		assertFalse("Checking edge insertion cannot add an edge between a node and itself", dag.putEdge(20, 10));
		assertFalse("Checking null edge insertion does not work", dag.putEdge(null, null));
	}

	@Test
	public void testGet(){
		//checks to see if the get function operates as described
		assertEquals("Testing that the key is returned if an item is in the tree", (Integer)10 , lca.get(10));
		assertNull("Testing that get returns null when item is not in the tree", lca.get(30));
		assertNull("Testing that get returns null when item is null", lca.get(null));

		//checks to see if the get function operates as needed for the graph
		assertEquals("Testing that the key is returned if an item is in the graph", (Integer)10 , dag.get(10));
		assertNull("Testing that get returns null when item is not in the graph", dag.get(30));
		assertNull("Testing that get returns null when item is null", dag.get(null));
	}

	@Test
	public void testExist(){
		//tests get as well, as exists simply calls get

		//checks to see if the exists function operates as described
		assertTrue("Testing that exists returns that an item in the tree", lca.exists(10));
		assertFalse("Testing that exists returns null when item is not in the tree", lca.exists(60));
		assertFalse("Testing that exists returns null when item is null", lca.exists(null));

		//checks to see if the exists function operates as needed for the graph
		assertTrue("Testing that exists returns that an item in the graph", dag.exists(10));
		assertFalse("Testing that exists returns null when item is not in the graph", dag.exists(60));
		assertFalse("Testing that exists returns null when item is null", dag.exists(null));
	}

	@Test
	public void testSameInputs(){
		//check two common ancestors, where inputs are the same, and exist in the tree
		assertEquals("Checking two common ancestors, where inputs are the same", (Integer)3 , lca.getLowestCommonAncestor(3, 3));

		//graph test version
		Set<Integer> answer = new HashSet<Integer>();
		answer.add((Integer)3);
		assertEquals("Checking two common ancestors, where inputs are the same", answer , dag.getLowestCommonAncestor(3, 3));
	}

	@Test
	public void testMissingInput(){
		//check where one input does not exist
		assertNull("Checking where one input does not exist in the tree", lca.getLowestCommonAncestor(4, 5));

		//graph test version
		assertNull("Checking where one input does not exist in the graph", dag.getLowestCommonAncestor(4, 5));
	}

	@Test
	public void testAncestorInput(){
		//check where one is an ancestor of the other
		assertEquals("Checking two common ancestors, where one is an ancestor of the other", (Integer)7, lca.getLowestCommonAncestor(7, 10));

		//graph test version
		Set<Integer> answer = new HashSet<Integer>();
		answer.add((Integer)7);
		assertEquals("Checking two common ancestors, where one is an ancestor of the other in the graph", answer, dag.getLowestCommonAncestor(7, 10));
	}

	@Test
	public void testAncestorRoot(){
		//check where both are the descendant of the root, with that as the LCA
		assertEquals("Checking two common ancestors, where LCA is the root", (Integer)4, lca.getLowestCommonAncestor(3, 9));

		//graph test version
		Set<Integer> answer = new HashSet<Integer>();
		answer.add((Integer)4);
		assertEquals("Checking two common ancestors, where LCA is the root of the graph", answer, dag.getLowestCommonAncestor(3, 9));
	}

	@Test
	public void testAncestorNormal(){
		//check where both are the descendant of the root, with that NOT as the LCA
		assertEquals("Checking two common ancestors, where LCA is not the root", (Integer)7, lca.getLowestCommonAncestor(6, 10));

		//graph test version
		Set<Integer> answer = new HashSet<Integer>();
		answer.add((Integer)7);
		assertEquals("Checking two common ancestors, where LCA is not the root of the graph", answer, dag.getLowestCommonAncestor(6, 10));

		//testing a graph not in the form of a binary search tree and not of type integer
		DAG<Character, Character> fullDAG = new DAG<Character, Character>();
		/*
			Formatted inserted data for illustration
						A	  B
					  | / |
				C   D   E
				| /\| /
				F   G
		*/
		// add data into the directed acyclic graph
		fullDAG.put('A', 'A');
		fullDAG.put('B', 'B');
		fullDAG.put('C', 'C');
		fullDAG.put('D', 'D');
		fullDAG.put('E', 'E');
		fullDAG.put('F', 'F');
		fullDAG.put('G', 'G');

		fullDAG.putEdge('A','D');
		fullDAG.putEdge('B','D');
		fullDAG.putEdge('B','E');
		fullDAG.putEdge('C','F');
		fullDAG.putEdge('C','G');
		fullDAG.putEdge('D','F');
		fullDAG.putEdge('D','G');
		fullDAG.putEdge('E','G');

		Set<Character> dagAnswer = new HashSet<Character>();
		dagAnswer.add((Character)'C');
		dagAnswer.add((Character)'D');
		assertEquals("Checking two common ancestors of a proper directed acyclic graph", dagAnswer, fullDAG.getLowestCommonAncestor('F', 'G'));

	}

	@Test
	public void testAncestorNull(){
		assertNull("Checking Lowest common ancestor, where parameter is null", lca.getLowestCommonAncestor(6, null));

		//graph test version
		assertNull("Checking Lowest common ancestor, where parameter is null", dag.getLowestCommonAncestor(6, null));
	}
}
